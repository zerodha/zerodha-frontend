module.exports = {
  'transpileDependencies': [
    'vuetify'
  ],

  devServer: {
    proxy: {
      '^/api': {
        target: 'http://localhost:3000',
        changeOrigin: true
      },
      '^/proxy-nse/': {
        target: 'https://www1.nseindia.com',
        pathRewrite: {
          '^/proxy-nse/': '/'
        },
        changeOrigin: true
      }
    }
  }
}
