import axios from 'axios'

function * extractGroupsIt (str, regex) {
  let onGoingResults
  while ((onGoingResults = regex.exec(str))) {
    yield onGoingResults.groups
  }
}

const extractGroups = (...args) => [...extractGroupsIt(...args)]

const extractLinksFromLinkHeader = linkHeader =>
  extractGroups(linkHeader, /<(?<url>\S+)>; rel="(?<rel>\S+)"/g)
    .reduce((acc, t) => ({ ...acc, [t.rel]: t.url }), {})

export default class CompanyRepository {
  static async _getPaginatedResource (...args) {
    const response = await axios.get(...args)
    const { next, last } = extractLinksFromLinkHeader(response.headers['link'])

    return {
      currentResults: response.data,
      fetchNextResults: next && (() => CompanyRepository._getPaginatedResource(next)),
      fetchLastResults: last && (() => CompanyRepository._getPaginatedResource(last))
    }
  }

  static searchListedEquities ({ symbol, listingPeriod, start = 0, limit = 15 }) {
    return CompanyRepository._getPaginatedResource('/api/equities', {
      params: { symbol, 'listing-period': listingPeriod, start, limit }
    })
  }

  static async searchCompanies (query) {
    return (await axios.get('/api/companies', {
      params: { query }
    })).data
  }

  static async fetchCompanyInsights ({ symbol }) {
    const insights = (await axios.get(`/api/companies/${symbol}/insights`)).data
    return {
      ...insights,
      corporateActions: {
        ...insights.corporateActions,
        actions: insights.corporateActions.actions && insights.corporateActions.actions.map(
          (action) => ({
            ...action, exDate: new Date(action.exDate)
          })
        )
      }
    }
  }
}
