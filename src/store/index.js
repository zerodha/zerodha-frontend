import Vue from 'vue'
import Vuex from 'vuex'
import { companyStore } from './CompanyStore'
import { companyInsightsStore } from './CompanyInsightsStore'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    companyStore,
    companyInsightsStore
  }
})
