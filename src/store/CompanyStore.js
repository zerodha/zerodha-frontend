import { debounce } from 'lodash'
import CompanyRepository from '../repositories/CompanyRepository'

const initialState = {
  companyMetas: {
    isLoading: false,
    value: []
  },
  validListingPeriods: Object.freeze({
    L1M: 'Last 1 Month',
    L6M: 'Last 6 Months',
    L1Y: 'Last 1 Year',
    L2Y: 'Last 2 Years',
    L5Y: 'Last 5 Years',
    MT5Y: 'More than 5 Years'
  }),
  equities: {
    headers: Object.freeze([
      { text: 'Symbol', value: 'symbol' },
      { text: 'Name', value: 'companyName' },
      { text: 'Listing Dt.', value: 'listingDate' },
      { text: 'Market Lot', value: 'marketLot' },
      { text: 'Face value', value: 'faceValue' },
      { text: 'Industry', value: 'industry' },
      { text: 'Paid up value', value: 'paidUpValue' },
      { text: 'ISIN', value: 'isin' }
    ]),
    isLoading: false,
    value: []
  }
}

const _searchCompanies = async ({ commit }, query) => {
  commit('SET_COMPANY_METAS_STATE', { isLoading: true })
  const results = await CompanyRepository.searchCompanies(query)
  commit('SET_COMPANY_METAS_STATE', {
    isLoading: false,
    value: results.map(t => ({ ...t, companyLabel: `${t.symbol} - ${t.companyName}` }))
  })
}

const searchCompaniesDebounced = debounce(_searchCompanies, 700)

export const companyStore = {
  namespaceed: true,
  state: initialState,
  mutations: {
    SET_COMPANY_METAS_STATE (state, companyMetas) {
      state.companyMetas = { ...companyMetas }
    },
    UPDATE_EQUITY_SEARCH_RESULTS (state, delta = { }) {
      const appendedResults = [...state.equities.value, ...(delta.currentResults || [])]
      state.equities = { ...state.equities, value: appendedResults, ...delta }
    }
  },
  actions: {
    searchCompaniesAfterInputIsFinished ({ commit }, query) {
      commit('SET_COMPANY_METAS_STATE', { isLoading: true,
        // TODO: worst hack to display waiting to finish message
        value: [{ companyLabel: query + ' - Waiting for you to finish...' }] })
      searchCompaniesDebounced({ commit }, query)
    },

    async searchEquities ({ commit }, { symbol, listingPeriod }) {
      commit('UPDATE_EQUITY_SEARCH_RESULTS', { isLoading: true, value: [], fetchNextResults: null })
      const results = await CompanyRepository.searchListedEquities({ symbol, listingPeriod })
      commit('UPDATE_EQUITY_SEARCH_RESULTS', {
        isLoading: false,
        ...results
      })
    },

    async loadNextEquitySearchResults ({ commit, state }) {
      const results = await state.equities.fetchNextResults()
      commit('UPDATE_EQUITY_SEARCH_RESULTS', {
        isLoading: false,
        ...results
      })
    }
  },
  modules: {}
}
