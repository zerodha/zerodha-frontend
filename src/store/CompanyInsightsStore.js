import { cloneDeep } from 'lodash'
import CompanyRepository from '../repositories/CompanyRepository'

const EMPTY_INSIGHTS = Object.freeze({
  companyInfo: {},
  corporateActions: {},
  corporateAnnouncements: []
})
const initialState = Object.freeze({
  symbolVsInsights: {},
  insights: {
    isLoading: false,
    value: EMPTY_INSIGHTS
  }
})

export const companyInsightsStore = {
  namespaceed: true,
  state: cloneDeep(initialState),
  mutations: {
    SET_COMPANY_INSIGHTS (state, companyInsights) {
      state.insights = { ...state.insights, ...companyInsights }
    }
  },
  actions: {
    async fetchInsights ({ commit, state: { symbolVsInsights } }, { symbol }) {
      commit('SET_COMPANY_INSIGHTS', { isLoading: true, value: symbolVsInsights[symbol] || cloneDeep(EMPTY_INSIGHTS) })
      symbolVsInsights[symbol] = await CompanyRepository.fetchCompanyInsights({ symbol })
      commit('SET_COMPANY_INSIGHTS', { isLoading: false, value: symbolVsInsights[symbol] })
    }

  }
}
