import Vue from 'vue'
import App from './App.vue'
import store from './store'
import vuetify from './plugins/vuetify'
import io from 'socket.io-client'

Vue.config.productionTip = false

new Vue({
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

// TODO: Extra code to spin up backend and proxy

const spinUpCodesandbox = (id) => {
  const socket = io(`https://sse.codesandbox.io`, {
    autoConnect: false,
    transports: ['websocket'],
    reconnectionAttempts: 5,
    reconnectionDelayMax: 32000
  })

  socket.on('connect', () => {
    socket.emit('sandbox', { id })
    socket.emit('sandbox:start')
  })

  socket.on('sandbox:log', ({ data }) => {
    console.log(`codesandbox:${id}`, data)
  })

  socket.on('sandbox:error', ({ message, unrecoverable }) => {
    if (unrecoverable) {
      console.error(`codesandbox:${id}`, message)
      socket.close()
      spinUpCodesandbox(id)
    }
  })

  socket.connect()
}

spinUpCodesandbox('2pwjj')
